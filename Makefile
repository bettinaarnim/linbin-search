default: build

build: searcher

searcher: main.o

main.o: main.cpp
	g++ -Wall main.cpp -o searcher

feed: gen.py searcher
	python gen.py