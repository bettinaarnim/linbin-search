#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

#ifdef TIME_TEST
#include <time.h>
#endif

typedef long long sint64;

using namespace std;

int main( void ) {

	// our intermediary input string
	string input;


	// our arrays of values
	sint64* Field = NULL, *QuerySet = NULL;
	sint64 Value;
	int sz_Field = 0, sz_QuerySet = 0;


	// get the initial array sizes
	cin >> sz_Field;
	cin >> sz_QuerySet;

	vector<sint64> FieldV;
	vector<bool>   LinearResults;
	vector<bool>   BinaryResults;
	Field = new sint64[sz_Field];
	QuerySet = new sint64[sz_QuerySet];

	// we store our input values in a dynamic array to facilitate sorting
	for (sint64 i = 0; i < sz_Field; i++) {
		cin >> Value;

		FieldV.push_back(Value);
	}

	// we pre-allocate results arrays to minimize impact during time trials
	for (sint64 j = 0; j < sz_QuerySet; j++) {
		cin >> QuerySet[j];

		LinearResults.push_back(false);
		BinaryResults.push_back(false);
	}


	// sort the Field
	sort(FieldV.begin(), FieldV.end());

	// and copy back into our allocated stack space
	for (sint64 i = 0; i < sz_Field; i++) {
		Field[i] = FieldV[i];
	}


	//////////////////////////////////
	// perform the linear search first

	sint64 num_matches_linear = 0, num_matches_binary = 0;

	cout<<"Linear Search: \n";


#ifdef TIME_TEST
	clock_t lbefore, lafter;
	clock_t bbefore, bafter;

	lbefore = clock();
#endif

	// for each item in the query set
	for (sint64 i = 0; i < sz_QuerySet; i++) {
		bool Found = false;
		sint64 cache = QuerySet[i];

		// search sequentially through our search field
		for (sint64 j = 0; j < sz_Field; j++) {
			if (Field[j] == cache) {
				j = sz_Field;
				Found = true;
				num_matches_linear++;
			}
		}

		// and set the appropriate results flag
		LinearResults[i] = Found;
	}

#ifdef TIME_TEST
	lafter = clock();
#endif

	// if requested, output string of 'True' and 'False'

#ifndef TIME_TEST
	for (unsigned int i = 0; i < LinearResults.size(); i++)
		cout << string((LinearResults[i])?"True\n" : "False\n");
#endif


	///////////////////////////////////////
	// perform binary search second

	cout<<"Binary Search: \n";

#ifdef TIME_TEST
	bbefore = clock();
#endif

	// for each item in our query set
	for (sint64 i = 0; i < sz_QuerySet; i++) {
		sint64 cache = QuerySet[i];
		bool Found = false;

		sint64 First, Last, Middle;

		First = 0;
		Last = sz_Field-1;
		Middle = (First+Last) / 2;

		// we use a search 'set' consisting of start, query, and end elements

		while( Found == false && First<=Last ) {

				// branch appropriately to our comparison
				if (Field[Middle] < cache) {
					First = Middle+1;
					Middle = (First + Last) / 2;
				} else if (Field[Middle] > cache) {
					Last = Middle-1;
					Middle = (First + Last) / 2;
				} else {
					// if we're not bigger or smaller, we found the item!
					Found = true;
					num_matches_binary ++;
				}
			}

		BinaryResults[i] = Found;
	}

#ifdef TIME_TEST
	bafter = clock();
#endif
	
	// and print the "True" "False" bit
#ifndef TIME_TEST
	for (unsigned int i = 0; i < BinaryResults.size(); i++)
		cout << string((BinaryResults[i])?"True\n" : "False\n");
#endif


	/////////////////////////////////////////////////////
	// This last bit was useful for time trialing

#ifdef TIME_TEST
	cout << "\n\n";

	cout << "Searched " << sz_Field << " Elements for " << sz_QuerySet << " Query items.\n\n";

	cout << "Time Taken: " << float(lafter - lbefore) / CLOCKS_PER_SEC << " seconds\n";
	cout << "  Linear Search found " << num_matches_linear << " matches\n\n";

	cout << "Time Taken: " << float(bafter - bbefore) / CLOCKS_PER_SEC << " seconds\n";
	cout << "  Binary Search found " << num_matches_binary << " matches\n\n";
#endif


	// free our allocated memory

	delete [] Field;
	delete [] QuerySet;
}